#!/bin/bash

# Run only if root
if [ "$EUID" -ne 0 ]; then
  echo You must be root to install Gnome Config VFX
  exit 1
fi

# Convenient Vars
current_dir=`dirname "$0"`
nautilus_extensions_dir=/usr/share/nautilus-python/extensions
gnome_shell_extentions_dir=/usr/share/gnome-shell/extensions
glib_schemas_dir=/usr/share/glib-2.0/schemas

################################
# Install Dependencies
echo ------------------------------------
echo Installing required dependencies...

dnf install epel-release -y
dnf install gnome-tweaks gnome-extensions-app nautilus-extensions nautilus-python python3-gobject -y


################################
### Nautilus

# Install Nautilus Extension
mkdir -p $nautilus_extensions_dir
cp -r "${current_dir}/vendor/nautilus-extensions/nautilus-copy-path" $nautilus_extensions_dir
cp "${current_dir}/vendor/nautilus-extensions/nautilus-copy-path.py" $nautilus_extensions_dir

################################
### Install Gnome Extensions
echo ------------------------------------
echo Installing Gnome shell Extensions...

# Install Dash to Panel
cp -r "${current_dir}/vendor/gnome-extensions/dash-to-panel@jderose9.github.com" $gnome_shell_extentions_dir
cp "${current_dir}/vendor/gnome-extensions/dash-to-panel@jderose9.github.com/schemas/org.gnome.shell.extensions.dash-to-panel.gschema.xml" $glib_schemas_dir

# Install System Monitor
cp -r "${current_dir}/vendor/gnome-extensions/system-monitor@paradoxxx.zero.gmail.com" $gnome_shell_extentions_dir
cp "${current_dir}/vendor/gnome-extensions/system-monitor@paradoxxx.zero.gmail.com/schemas/org.gnome.shell.extensions.system-monitor.gschema.xml" $glib_schemas_dir

# Install Arch Menu
cp -r "${current_dir}/vendor/gnome-extensions/arcmenu@arcmenu.com" $gnome_shell_extentions_dir
cp "${current_dir}/vendor/gnome-extensions/arcmenu@arcmenu.com/schemas/org.gnome.shell.extensions.arcmenu.gschema.xml" $glib_schemas_dir

# Install Gnome 4x UI Improvements
cp -r "${current_dir}/vendor/gnome-extensions/gnome-ui-tune@itstime.tech" $gnome_shell_extentions_dir
cp "${current_dir}/vendor/gnome-extensions/gnome-ui-tune@itstime.tech/schemas/org.gnome.shell.extensions.gnome-ui-tune.gschema.xml" $glib_schemas_dir

# Install Blur My Shell
cp -r "${current_dir}/vendor/gnome-extensions/blur-my-shell@aunetx" $gnome_shell_extentions_dir
cp "${current_dir}/vendor/gnome-extensions/blur-my-shell@aunetx/schemas/org.gnome.shell.extensions.blur-my-shell.gschema.xml" $glib_schemas_dir

# Compile Schemas
glib-compile-schemas $glib_schemas_dir

################################
### dconf
echo ------------------------------------
echo Installing dconf profile file

# Copy dconf profile file
cp -v "${current_dir}/01-gnome_config_vfx" /etc/dconf/db/local.d

# Update dconf
dconf update
